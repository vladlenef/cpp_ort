#include <iostream>

using namespace std;

int main() {
    setlocale (LC_ALL, "Russian");
    
    int count;
    cout << "����������: ";
    cin  >> count;
    
    int summary = 0;
    int max = 0;
    int min = 100;
    for (int i = 0; i < count; i++) {

        int age;
        cout << "������� " << i + 1 << "-�� ���������� ";
        cin  >> age;

        summary += age;

        if (age > max) {
            max = age;
        }
        if (age < min) {
            min = age;
        }
    }
    
    float average = static_cast<float>(summary) / count;
    
    cout << "������    " << max << endl
         << "�������   " << min << endl
         << "������� " << average;
    
    return 0;
}
