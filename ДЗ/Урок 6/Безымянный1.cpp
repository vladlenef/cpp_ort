#include <iostream>
using namespace std;
 
void RekursSplit(int val);
 
int main()
{
    int val;
    while(true)
    {
        cout<<"Enter number : ";
        if(!(cin>>val))
        {
            cout<<"Incorrect input"<<endl;
            cin.clear();
            cin.sync();
        }
        else
        RekursSplit(val);
    }
    return 0;
}
 
void RekursSplit(int val)
{
    if(val < 0)
        val *= - 1;
    cout<<val % 10<<" ";
    if(val /= 10)
        RekursSplit(val);
    else
        cout<<endl;
}
